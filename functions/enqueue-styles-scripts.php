<?php

/*
	Enqueue Styles & Scripts
*/

// Enqueue custom styles and scripts
function esa_enqueue_styles_and_scripts() {
    // Register and noConflict jQuery 3.6.0
    wp_register_script( 'jquery.3.6.0', 'https://code.jquery.com/jquery-3.6.0.min.js' );
    wp_add_inline_script( 'jquery.3.6.0', 'var jQuery = $.noConflict(true);' );

    $dir = get_template_directory_uri();

    // Add style.css and third-party css
    wp_enqueue_style( 'yosemite', $dir. '/yosemite.css', array(), false, 'all');

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script('yosemite-plugins', $dir . '/js/plugins.js', array('jquery.3.6.0'), false, true );
    wp_enqueue_script('yosemite-scripts', $dir . '/js/site.js', array('jquery.3.6.0'), false, true );
}
add_action( 'wp_enqueue_scripts', 'esa_enqueue_styles_and_scripts' );