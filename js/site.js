(function ($, window, document, undefined) {

	$(document).ready(function($) {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});
		

		// Header - Hamburger Menu
		$('.nav-trigger').on('click', function() {
			$('body').toggleClass('nav-open');

			return false;
		});


		// Before and After Slider
		$('.ba-slider').beforeAfter();


		// Key Findings - Smooth Scroll
		$('.key-findings a, .smooth').smoothScroll();


		// Video Overlay Show
		$('.video-trigger').click(function(){
			var videoID = $(this).data('video-id');
			var videoType = $(this).data('video-type');

			if(videoType == "vimeo") {
				$('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" src="https://player.vimeo.com/video/' + videoID + '?autoplay=1&controls=1&byline=0&playsinline=1&quality=1080p" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
			} else {
				$('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" allowfullscreen frameborder="0" allowTransparency="true" src="https://www.youtube.com/embed/' + videoID + '?autoplay=1&modestbranding=1&rel=0"></iframe>');
			}
			
			$('#video-overlay').fadeIn(200);
			$('body').addClass('video-overlay-open');

			return false;
		});


		// Video Overlay Hide
		$('.video-close-btn').click(function(){
			$('#video-overlay').fadeOut(200);
			$('body').removeClass('video-overlay-open');
			$('#video-overlay .video-frame .content').empty();

			return false;
		});


		// SmoothScroll to document category
		$('.documents-categories a').smoothScroll();


		// Live Filter/Search for documents with keywords
		$('#documents-filter').on('keyup', function() {
			var value = $(this).val().toLowerCase();

			$("#documents-table tr.file").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});


		// Disable filter button
		$('.filter button').on('click', function(){
			return false;
		});

		
		// Disable dropdown link
		$('.dropdown:not(.main-link) .top-level-link').on('click', function(){
			return false;
		});


		// Alternatives: Tabs
		$('.tab-links a').on('click', function(){
			var hash = $(this).attr('href');

			$('.tab-links a').removeClass('active');
			$(this).toggleClass('active');

			$('section.alternative').removeClass('active');
			$(hash).addClass('active');
			
			return false;
		});


		// Alternatives: Toggle Bar
		$('.toggle-bar').on('click', function(){
			$(this).closest('.toggle').toggleClass('open');

			return false;
		});

		
		$(document).keyup(function(e) {

			// Escape			
			if (e.keyCode == 27) {
				$('body').removeClass('video-overlay-open');

				$('#video-overlay').fadeOut(200);
				$('#video-overlay .video-frame .content').empty();
			}

		});
			
	});

})(jQuery, window, document);