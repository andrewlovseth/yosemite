<?php if(!is_page_template('templates/homepage.php')): ?>
	<section class="page-header grid">
		<h1><?php echo get_field('project_type', 'options'); ?></h1>
		<h2><?php echo get_field('project_name', 'options'); ?></h2>
	</section>
<?php endif; ?>