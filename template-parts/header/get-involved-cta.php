<?php 
	$link = get_field('get_involved_cta', 'options');
	if( $link ): 
	$link_url = $link['url'];
	$link_title = $link['title'];
	$link_target = $link['target'] ? $link['target'] : '_self';
 ?>

 	<li class="get-involved-cta cta">
 		<a class="btn border-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
 	</li>

<?php endif; ?>