<?php global $is_IE; if ( $is_IE ): ?>

    <section class="internet-explorer">
        <div class="banner">
            <div class="copy p3">
                <p>This site doesn't have full support for Internet Explorer. Please use a modern web browser such as <a href="https://www.microsoft.com/edge/" target="_blank">Edge</a>, <a href="https://www.google.com/chrome/" target="_blank">Chrome</a>, <a href="https://mozilla.org/firefox/" target="_blank">Firefox</a>, or <a href="https://support.apple.com/downloads/safari" target="_blank">Safari</a>.</p>
            </div>
        </div>
    </section>

<?php endif; ?>
