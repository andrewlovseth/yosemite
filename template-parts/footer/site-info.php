<div class="site-info">
	<div class="logo">
		<a href="<?php echo site_url(); ?>">
			<img src="<?php $image = get_field('site_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="project-info">
		<h1><?php echo get_field('project_name', 'options'); ?></h1>
		<h2><?php echo get_field('project_type', 'options'); ?></h2>

		<p>
			<?php echo get_field('preparation_info', 'options'); ?><br/>
			<?php echo get_field('preparation_date', 'options'); ?>
		</p>
	</div>
</div>