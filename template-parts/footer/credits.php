<div class="credits">
	<a href="<?php echo get_field('esa_link', 'options'); ?>" rel="external">
		<img src="<?php $image = get_field('esa_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</a>
</div>