<section class="hero">
	<div class="photo">
		<div class="content">
			<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
	</div>

	<div class="info grid">
		<div class="headline">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>		
</section>