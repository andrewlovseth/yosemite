<div class="social-links">
    <?php if(have_rows('social', 'options')): while(have_rows('social', 'options')): the_row(); ?>
    
    <div class="link">
        <a href="<?php echo get_sub_field('link'); ?>" class="<?php echo sanitize_title_with_dashes(get_sub_field('network')); ?>" rel="external">
            <span class="icon"></span>
            <span class="label"><?php echo get_sub_field('network'); ?></span>
        </a>
    </div>

    <?php endwhile; endif; ?>
</div>