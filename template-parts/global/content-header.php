<section class="content-header grid">
	<div class="headline">
		<h3 class="h4"><?php echo get_field('page_header_headline'); ?></h3>
	</div>

	<div class="copy p2">
		<?php echo get_field('page_header_copy'); ?>
	</div>
</section>