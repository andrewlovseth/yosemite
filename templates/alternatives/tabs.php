<section class="tabs grid">
    <div class="tab-links">
        <?php if(have_rows('tabs')): $i = 1; while(have_rows('tabs')): the_row(); ?>

            <a href="<?php echo get_sub_field('hash'); ?>"<?php if($i == 1): ?> class="active"<?php endif; ?>>
                <?php echo get_sub_field('label'); ?>
            </a>

        <?php $i++; endwhile; endif; ?>
    </div>
</section>
