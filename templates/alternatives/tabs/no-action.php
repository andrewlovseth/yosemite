<section class="alternative grid" id="<?php echo sanitize_title_with_dashes(get_field('no_action_title')); ?>">
    <div class="section-header headline">
        <h2 class="h3"><?php echo get_field('no_action_title'); ?></h2>
    </div>    
    
    <?php if(have_rows('no_action_sections')): $i = 1; while(have_rows('no_action_sections')) : the_row(); ?>
        <?php
            $args = array('count' => $i);
            get_template_part('templates/alternatives/content-sections', NULL, $args);
        ?>
    <?php $i++; endwhile; endif; ?>
</section>
