<?php if( get_row_layout() == 'headline_and_copy' ): ?>

    <div class="headline-and-copy toggle<?php if ($args['count'] == 1): ?> open<?php endif; ?>">
        <div class="headline toggle-bar">
            <h3 class="h5"><?php echo get_sub_field('headline'); ?></h3>
        </div>

        <div class="info">
            <div class="copy p2">
                <?php echo get_sub_field('copy'); ?>
            </div>
        </div>                
    </div>

<?php endif; ?>