<section class="location grid">
	<div class="headline">
		<h3 class="h4"><?php echo get_field('location_headline'); ?></h3>
    </div>
    
	<div class="copy p2">
		<?php echo get_field('location_copy'); ?>
    </div>
    
    <div class="interactive">
        <div class="content">
            <?php echo get_field('location_map'); ?>	
        </div>
    </div>
</section>