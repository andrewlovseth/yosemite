<section class="project-objectives grid">
	<div class="headline">
		<h3 class="h4"><?php echo get_field('project_objectives_headline'); ?></h3>
	</div>

	<div class="copy p2">
		<?php echo get_field('project_objectives_copy'); ?>
	</div>
</section>