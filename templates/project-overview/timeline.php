<section class="timeline grid">
	<div class="headline">
		<h3 class="h4"><?php echo get_field('timeline_headline'); ?></h3>
	</div>

    <?php if(have_rows('timeline')): ?>
        <div class="infographic">
            <?php while(have_rows('timeline')): the_row(); ?>
    
                <div class="entry sub-grid">
                    <div class="date">
                        <h5><?php echo get_sub_field('date'); ?></h5>
                    </div>

                    <div class="info">
                        <div class="headline">
                            <h3><?php echo get_sub_field('headline'); ?></h3>
                        </div>

                        <div class="copy p3">
                            <?php echo get_sub_field('copy'); ?>
                        </div>
                    </div>
                </div>

            <?php endwhile; ?>        
        </div>
    <?php endif; ?>

</section>