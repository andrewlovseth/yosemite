<section class="summary grid">
	<div class="headline">
		<h3 class="h4"><?php echo get_field('summary_headline'); ?></h3>
	</div>

	<div class="copy p2">
		<?php echo get_field('summary_copy'); ?>
	</div>
</section>