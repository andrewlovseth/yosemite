<section class="project-background grid">
	<div class="headline">
		<h3 class="h4"><?php echo get_field('project_background_headline'); ?></h3>
	</div>

	<div class="copy p2">
		<?php echo get_field('project_background_copy'); ?>
	</div>
</section>