<?php

/*
	Template Name: Environmental Impacts
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>

		<?php get_template_part('templates/environmental-impacts/key-findings'); ?>

		<?php get_template_part('templates/environmental-impacts/three-column-text'); ?>

		<?php get_template_part('templates/environmental-impacts/arcgis'); ?>

		<?php get_template_part('templates/environmental-impacts/map'); ?>

		<?php get_template_part('templates/environmental-impacts/gallery'); ?>

		<?php get_template_part('templates/environmental-impacts/video'); ?>

		<?php get_template_part('templates/environmental-impacts/before-and-after'); ?>

		<?php get_template_part('templates/environmental-impacts/audio'); ?>

	<?php endwhile; endif; ?>

	<?php get_template_part('template-parts/global/video-overlay'); ?>

<?php get_footer(); ?>