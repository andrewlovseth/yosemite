<div class="subscribe">
	<div class="headline">
		<h5 class="h6"><?php echo get_field('subscribe_headline'); ?></h5>
	</div>

	<div class="copy p4">
		<?php echo get_field('subscribe_copy'); ?>
	</div>

	<div class="form-wrapper">
		<?php
			$shortcode = get_field('subscribe_form');
			if($shortcode) {
				echo do_shortcode($shortcode);
			}
		?>
	</div>
</div>