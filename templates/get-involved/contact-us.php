<div class="contact-us">
	<div class="headline">
		<h5 class="h6"><?php echo get_field('contact_us_headline'); ?></h5>
	</div>

	<div class="copy p4">
		<?php echo get_field('contact_us_copy'); ?>
	</div>

	<?php if(get_field('contact_us_email')): ?>
		<div class="email p4">
			<p>
				<strong>Email</strong>
				<a href="mailto:<?php echo get_field('contact_us_email'); ?>"><?php echo get_field('contact_us_email'); ?></a>
			</p>
		</div>
	<?php endif; ?>

	<?php if(get_field('contact_us_phone')): ?>
		<div class="phone p4">
			<p>
				<strong>Phone</strong>
				<a href="tel:<?php echo get_field('contact_us_phone'); ?>"><?php echo get_field('contact_us_phone'); ?></a>
			</p>
		</div>
	<?php endif; ?>
</div>