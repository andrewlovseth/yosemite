<?php if(have_rows('events')): ?>

	<section class="events">

		<?php while(have_rows('events')) : the_row(); ?>

			<?php if( get_row_layout() == 'event' ): ?>

				<div class="event">
					<div class="date cal">
						<?php
							$date_string = get_sub_field('date');
							$date = DateTime::createFromFormat('F j, Y', $date_string);
							$month = $date->format('M');
							$day = $date->format('j');
						?>

						<span class="month"><?php echo $month; ?></span>
						<span class="day"><?php echo $day; ?></span>
							
						</span>
						
					</div>

					<div class="info">
						<div class="title headline">
							<h3 class="h5"><?php echo get_sub_field('title'); ?></h3>
						</div>

						<div class="description copy p3">
							<?php echo get_sub_field('description'); ?>
						</div>

						<div class="meta">
							<?php if(get_sub_field('date')): ?>
								<div class="date p3">
									<p>
										<strong class="key">Date</strong>
										<span class="value"><?php echo get_sub_field('date'); ?></span>
									</p>	
								</div>
							<?php endif; ?>

							<?php if(get_sub_field('time')): ?>
								<div class="time p3">
									<p>
										<strong class="key">Time</strong>
										<span class="value"><?php echo get_sub_field('time'); ?></span>
									</p>	
								</div>
							<?php endif; ?>

							<?php if(get_sub_field('location')): ?>
								<div class="location p3">
									<p>
										<strong class="key">Location</strong>
										<span class="value"><?php echo get_sub_field('location'); ?></span>
									</p>									
								</div>
							<?php endif; ?>

							<?php 
								$link = get_sub_field('link');
								if( $link ): 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
							 ?>
							 	<div class="link">
							 		<a class="event-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
							 	</div>
							<?php endif; ?>							
						</div>
					</div>
				</div>

			<?php endif; ?>

		<?php endwhile; ?>
		
	</section>

<?php endif; ?>