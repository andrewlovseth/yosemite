<?php

if( get_row_layout() == 'key_findings' ): 

$theme = get_sub_field('theme');

$className = 'key-findings impact-section grid';

if($theme) {
    $className .= ' ' . $theme . '-theme';
}

?>

	<section class="<?php echo esc_attr($className); ?>">

		<div class="photo">
			<div class="content">
				<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		</div>

		<div class="info">
			<div class="section-header">
				<h2><?php echo get_sub_field('headline'); ?></h2>
			</div>

			<div class="copy p1">
				<?php echo get_sub_field('copy'); ?>
			</div>
		</div>

	</section>

<?php endif; ?>

