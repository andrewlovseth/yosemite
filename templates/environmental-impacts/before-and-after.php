<?php

if( get_row_layout() == 'before_and_after' ): 

$theme = get_sub_field('theme');

$className = 'before-and-after impact-section grid';

if($theme) {
    $className .= ' ' . $theme . '-theme';
}

?>
	<section class="<?php echo esc_attr($className); ?>">

		<?php get_template_part('templates/environmental-impacts/section-header'); ?>

		<?php 
			$before = get_sub_field('before');
			$before_photo = $before['photo'];
			$before_caption = $before['caption'];

			$after = get_sub_field('after');
			$after_photo = $after['photo'];
			$after_caption = $after['caption'];
		?>

		<div class="ba-slider">
			<div class="caption caption-after">
				<?php echo $after_caption; ?>
			</div>

			<img src="<?php echo $after_photo['url']; ?>" alt="<?php echo $before_photo['alt']; ?>">   

			<div class="resize">
	  			<div class="caption caption-before">
					<?php echo $before_caption; ?>
				</div>
				<img src="<?php echo $before_photo['url']; ?>" alt="<?php echo $after_photo['alt']; ?>">     
			</div>

			<span class="handle"></span>
		</div>
	</section>

<?php endif; ?>