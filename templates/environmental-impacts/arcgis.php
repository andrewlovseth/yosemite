<?php

if( get_row_layout() == 'arcgis' ): 

$theme = get_sub_field('theme');

$className = 'arcgis impact-section grid';

if($theme) {
    $className .= ' ' . $theme . '-theme';
}

?>

	<section class="<?php echo esc_attr($className); ?>" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_header')); ?>">

		<?php get_template_part('templates/environmental-impacts/section-header'); ?>

		<div class="interactive">
			<div class="content">
				<?php echo get_sub_field('embed_code'); ?>
			</div>

			<div class="credit">
				<?php echo get_sub_field('caption'); ?>
			</div>			
		</div>

		<div class="copy p2">
			<?php echo get_sub_field('copy'); ?>

			<div class="footnotes">
				<?php echo get_sub_field('footnotes'); ?>
			</div>
		</div>

	</section>

<?php endif; ?>