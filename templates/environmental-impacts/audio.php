<?php

if( get_row_layout() == 'audio' ): 

$theme = get_sub_field('theme');

$className = 'audio impact-section grid';

if($theme) {
    $className .= ' ' . $theme . '-theme';
}

?>

	<section class="<?php echo esc_attr($className); ?>" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_header')); ?>">

		<?php get_template_part('templates/environmental-impacts/section-header'); ?>

		<div class="copy p2">
			<?php echo get_sub_field('copy'); ?>
		</div>

		<div class="audio-files">

			<?php if(have_rows('audio')): while(have_rows('audio')): the_row(); ?>
	 
				<div class="file">
					<div class="player">
						<?php
							$file = get_sub_field('file');
							$file_url = $file['url'];
							echo do_shortcode('[sc_embed_player fileurl="' . $file_url . '"]');
						?>
					</div>

					<div class="info">
						<div class="headline">
							<h3><?php echo get_sub_field('title'); ?></h3>
						</div>

						<div class="copy p3">
							<?php echo get_sub_field('description'); ?>
						</div>
					</div>				    
				</div>

			<?php endwhile; endif; ?>

		</div>

	</section>

<?php endif; ?>