<?php

if( get_row_layout() == 'video' ): 

$theme = get_sub_field('theme');

$className = 'video impact-section grid';

if($theme) {
    $className .= ' ' . $theme . '-theme';
}

?>

	<section class="<?php echo esc_attr($className); ?>" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_header')); ?>">

		<?php get_template_part('templates/environmental-impacts/section-header'); ?>

		<div class="video-player">

			<?php
				$video = get_sub_field('video');
				$service = $video['service'];
				$video_id = $video['id'];
				$thumbnail = $video['thumbnail'];
				$time = $video['time'];
				if($video): ?>
					

					<a class="video-trigger" href="#" data-video-service="<?php echo $service; ?>" data-video-id="<?php echo $video_id; ?>">
						<img src="<?php echo $thumbnail['sizes']['large']; ?>" alt="Thumbnail" />
						<span class="time"><?php echo $time; ?></span>
					</a>

			<?php endif; ?>

		</div>

		<div class="copy p2">
			<div class="credit">
				<?php echo get_sub_field('credit'); ?>
			</div>

			<?php echo get_sub_field('copy'); ?>
		</div>

	</section>

<?php endif; ?>