<?php

if( get_row_layout() == 'map' ): 

$theme = get_sub_field('theme');

$className = 'map impact-section grid';

if($theme) {
    $className .= ' ' . $theme . '-theme';
}

?>

	<section class="<?php echo esc_attr($className); ?>" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_header')); ?>">

		<?php get_template_part('templates/environmental-impacts/section-header'); ?>

		<div class="interactive">
			<div class="content">
				<?php echo get_sub_field('embed_code'); ?>	
			</div>
		</div>

		<div class="copy p2">
			<div class="credit">
				<?php echo get_sub_field('credit'); ?>
			</div>

			<?php echo get_sub_field('copy'); ?>
		</div>

	</section>

<?php endif; ?>