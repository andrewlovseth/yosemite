<?php

if( get_row_layout() == 'gallery' ): 

$theme = get_sub_field('theme');

$className = 'gallery impact-section grid';

if($theme) {
    $className .= ' ' . $theme . '-theme';
}

?>

	<section class="<?php echo esc_attr($className); ?>" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_header')); ?>">

		<?php get_template_part('templates/environmental-impacts/section-header'); ?>

		<div class="photo-gallery">
			<?php $photoIDs = ''; $galleryImages = get_sub_field('gallery'); if( $galleryImages ): ?>
				<?php foreach( $galleryImages as $galleryImage ): ?>
					
					<?php $photoIDs .= $galleryImage['ID'] . ','; ?>

				<?php endforeach; ?>
			<?php endif; ?>

			<?php echo do_shortcode('[gallery size="thumbnail" ids="' . $photoIDs . '"]'); ?>
		</div>

		<div class="copy p1">
			<?php echo get_sub_field('copy'); ?>
		</div>

	</section>

<?php endif; ?>
