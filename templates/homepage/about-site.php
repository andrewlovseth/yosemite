<section class="about-site grid home-outline">
	<div class="headline">
		<h4 class="h5"><?php echo get_field('about_site_headline'); ?></h4>
	</div>

	<div class="copy p2">
		<?php echo get_field('about_site_copy'); ?>
	</div>

	<?php get_template_part('template-parts/footer/download-report'); ?>
</section>