<section class="alternatives grid home-outline full-width-photo">
	<div class="photo">
		<div class="content">
			<img src="<?php $image = get_field('alternatives_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>			
	</div>

	<div class="info">
		<div class="headline">
			<h4 class="h5"><?php echo get_field('alternatives_sub_headline'); ?></h4>
			<h2 class="h2"><?php echo get_field('alternatives_headline'); ?></h2>
		</div>

		<div class="info-content">
			<div class="copy p2">
				<?php echo get_field('alternatives_copy'); ?>
			</div>

			<?php 
				$link = get_field('alternatives_link');
				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			 ?>

			 	<div class="cta underline">
			 		<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
			 	</div>

			<?php endif; ?>
		</div>


	</div>
</section>