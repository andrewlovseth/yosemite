<section class="impacts grid home-outline impacts-grid">
	<div class="info">
		<div class="headline">
			<h4 class="h5"><?php echo get_field('impacts_sub_headline'); ?></h4>
			<h2 class="h2"><?php echo get_field('impacts_headline'); ?></h2>
		</div>

		<div class="copy p2">
			<?php echo get_field('impacts_copy'); ?>
		</div>
	</div>

	<div class="icons">
		<?php if(have_rows('impacts_links')): $count = 1; while(have_rows('impacts_links')): the_row(); ?>
	 
		    <div class="impact impact-<?php echo $count; ?>">
		    	<?php 
					$link = get_sub_field('link');
					if( $link ): 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				 ?>

				 	<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
				 		<?php if(get_sub_field('icon')): ?>
				 			<span class="icon"><img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></span>
				 		<?php endif; ?>
				 		
				 		<span class="label"><?php echo esc_html($link_title); ?></span>
				 	</a>

				<?php endif; ?>
		    </div>

		<?php $count++; endwhile; endif; ?>
	</div>
</section>