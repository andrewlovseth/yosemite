<section class="project-description grid home-outline left-skinny-photo">
	<div class="photo">
		<div class="content">
			<img src="<?php $image = get_field('project_description_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>			
	</div>

	<div class="info">
		<div class="headline">
			<h4 class="h5"><?php echo get_field('project_description_sub_headline'); ?></h4>
			<h2 class="h2"><?php echo get_field('project_description_headline'); ?></h2>
		</div>

		<div class="copy p2">
			<?php echo get_field('project_description_copy'); ?>
		</div>

		<?php 
			$link = get_field('project_description_link');
			if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		 ?>

		 	<div class="cta underline">
		 		<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		 	</div>

		<?php endif; ?>

	</div>
</section>
