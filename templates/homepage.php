<?php

/*
	Template Name: Home
*/

get_header(); ?>

	<?php get_template_part('templates/homepage/hero'); ?>

	<?php get_template_part('templates/homepage/about-site'); ?>

	<?php get_template_part('templates/homepage/project-description'); ?>

	<?php get_template_part('templates/homepage/alternatives'); ?>

	<?php get_template_part('templates/homepage/impacts'); ?>
	
<?php get_footer(); ?>