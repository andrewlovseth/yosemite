<?php

/*
	Template Name: Project Overview
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<?php get_template_part('template-parts/global/content-header'); ?>

	<?php get_template_part('templates/project-overview/project-background'); ?>

	<?php get_template_part('templates/project-overview/location'); ?>

	<?php get_template_part('templates/project-overview/project-objectives'); ?>

	<?php get_template_part('templates/project-overview/timeline'); ?>

	<?php get_template_part('templates/project-overview/summary'); ?>

<?php get_footer(); ?>