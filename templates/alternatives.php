<?php

/*
	Template Name: Alternatives
*/

get_header(); ?>
	
	<?php get_template_part('templates/global/hero'); ?>

	<?php get_template_part('templates/global/content-header'); ?>

	<?php get_template_part('templates/alternatives/tabs'); ?>

	<?php get_template_part('template/alternatives/tab-sections'); ?>


<?php get_footer(); ?>