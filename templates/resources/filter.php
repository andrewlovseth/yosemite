<section class="filter grid">

	<form>
		<div class="filter-input">
			<label for="site-search h4">Filter by Keyword</label>
			<input type="search" placeholder="Search..." id="documents-filter" name="q"
			       aria-label="Filter documents by Keyword">

			<button>Filter</button>
		</div>
	</form>
	
</section>