<?php 

$stream_opts = [
    "ssl" => [
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ]
];

$spreadsheetID = get_field('sheet_id');
$tabID = get_field('tab_id');
$base_url = get_field('document_base_url');
$url = "https://spreadsheets.google.com/feeds/list/" . $spreadsheetID . "/" . $tabID . "/public/values?alt=json";
$data = file_get_contents($url, false, stream_context_create($stream_opts));
$feed = json_decode($data, true);
$entries = $feed['feed']['entry'];
$today_date = date('Ymd');

?>

<div class="documents-list">
	<table id="documents-table">
		<thead>
			<tr>
				<th class="cat">Category</th>
				<th class="date">Date</th>
				<th class="title">Title</th>
				<th class="author">Author</th>
				<th class="pdf">File</th>
			</tr>
		</thead>

		<?php if(have_rows('documents_categories')): ?>

			<tbody>
				<?php while(have_rows('documents_categories')): the_row(); ?>

					<?php
						$name = get_sub_field('name');
						$slug = get_sub_field('slug');
					?>
			 
				    <tr id="<?php echo $slug; ?>" class="header-row">
				    	<td class="cat-header" colspan="5"><?php echo $name; ?></td>				        
				    </tr>


					<?php
						$files = array_filter($entries, function ($entry) use ($slug) {
							return ($entry['gsx$category']['$t'] == $slug); }
						);

						if(!empty($files)): ?>

							<?php
								foreach ($files as $file): 
									$date = $file['gsx$date']['$t'];
									$title = $file['gsx$title']['$t'];
									$author = $file['gsx$author']['$t'];
									$file = $file['gsx$file']['$t'];
							?>

								<tr class="file">
									<td class="cat"><?php echo $name; ?></td>
									<td class="date"><?php echo $date; ?></td>
									<td class="title">
										<?php if($file !== ''): ?>
											<a href="<?php echo $base_url; ?><?php echo $file; ?>" rel="external"><?php echo $title; ?></a>
										<?php else: ?>
											<?php echo $title; ?>
										<?php endif; ?>													
									</td>
									<td class="author"><?php echo $author; ?></td>
									<td class="pdf">
										<?php if($file !== ''): ?>
											<a href="<?php echo $base_url; ?><?php echo $file; ?>" rel="external">PDF</a>
										<?php endif; ?>											
									</td>									
								</tr>

							<?php endforeach; ?>

						<?php else: ?>


						<?php endif; ?>

				<?php endwhile; ?>

			</tbody>

		<?php endif; ?>

	</table>
</div>