<?php

	$posts_page = get_option('page_for_posts');

?>

<section class="hero">
	<div class="photo">
		<div class="content">
			<img src="<?php $image = get_field('hero_image', $posts_page); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
	</div>

	<div class="info grid">
		<div class="headline">
			<?php if(is_category()): ?>
				<h1><?php single_cat_title(); ?></h1>
			<?php else: ?>
				<h1><?php echo get_the_title($posts_page); ?></h1>
			<?php endif; ?>
		</div>
	</div>		
</section>