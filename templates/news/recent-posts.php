<section class="recent-posts grid">
    <div class="section-header">
        <div class="headline">
            <h2 class="h5">Recent Posts</h2>
        </div>
    </div>

    <div class="posts-sub-grid">
        <?php
            $exclude_ids = array( $post->ID );
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 3,
                'post__not_in' => $exclude_ids,
                'order' => 'DESC',
                'orderby' => 'date'
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

            <article <?php post_class(); ?>>
                <div class="photo">
                    <div class="content">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('medium'); ?>
                        </a>
                    </div>
                </div>

                <div class="info">
                    <div class="meta">
                        <div class="date">
                            <span><?php echo get_the_time("F j, Y"); ?></span>
                        </div>
                    </div>
                    <div class="headline">
                        <h3 class="h6"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </div>

                    <div class="excerpt copy p4">
                        <?php the_excerpt(); ?>
                    </div>

                    <div class="cta">
                        <a class="btn" href="<?php the_permalink(); ?>">Read More</a>
                    </div>
                </div>
            </article>

        <?php endwhile; endif; wp_reset_postdata(); ?>
    </div>
</section>