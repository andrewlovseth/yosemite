<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

<?php $the_month = get_the_time( 'Y-m' ); if ( $last_month !== $the_month ): ?>

    <?php if($last_month !== null): ?>
        </div>
    <?php endif; ?>

    <?php if(get_the_time('Y') !== $display_year): ?>
        <div class="year year-<?php the_time('Y'); ?>">
            <div class="headline">
                <h4 class="h5"><?php $display_year = get_the_time('Y'); the_time('Y'); ?></h4>
            </div>
        </div>
    <?php endif; ?>

    <div class="month month-<?php echo $the_month; ?>">
        <aside class="month-header">
            <?php if(get_the_time('F') !== $display_month): ?>
                <h5><?php $display_month = get_the_time('F'); the_time('M'); ?></h5>
            <?php endif; ?>		
        </aside>
    <?php endif; $last_month = $the_month; ?>

    <article <?php post_class(); ?>>
        <div class="headline">
            <h3 class="h5"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        </div>

        <div class="excerpt copy p2">
            <?php the_excerpt(); ?>
        </div>

        <div class="cta">
            <a class="btn" href="<?php the_permalink(); ?>">Read More</a>
        </div>
    </article>

    <?php if ($wp_query->current_post +1 == $wp_query->post_count): ?>
        </div>

    <?php endif; ?>
<?php endwhile; endif; ?>