<?php

	$posts_page = get_option('page_for_posts');

?>

<div class="subscribe">
	<div class="headline">
		<h5 class="h6"><?php echo get_field('subscribe_headline', $posts_page); ?></h5>
	</div>

	<div class="copy p4">
		<?php echo get_field('subscribe_copy', $posts_page); ?>
	</div>

	<div class="form-wrapper">
		<?php
			/*$shortcode = get_field('subscribe_form', $posts_page);
			if($shortcode) {
				echo do_shortcode($shortcode);
			}
			*/
		?>

		<form>
			<div class="field name">
				<input type="text" placeholder="Name" />
			</div>

			<div class="field email">
				<input type="email" placeholder="Email Address" />
			</div>

			<div class="field submit">
				<input type="submit" value="Sign Up" />
			</div>
		</form>
	</div>

	<?php get_template_part('template-parts/global/social-links'); ?>
</div>