	</main> <!-- .site-content -->

	<footer class="site-footer grid">
		
		<?php get_template_part('template-parts/footer/site-info'); ?>

		<?php get_template_part('template-parts/footer/footer-nav'); ?>

		<?php get_template_part('template-parts/footer/download-report'); ?>

		<div class="utilties">
			<?php get_template_part('template-parts/footer/copyright'); ?>

			<?php get_template_part('template-parts/footer/credits'); ?>
		</div>



	</footer>

<?php wp_footer(); ?>

</div> <!-- .site -->
</body>
</html>