<?php 

/*

	Single News Post

*/

get_header(); ?>

    <section class="hero">
        <div class="photo">
            <div class="content">
                <?php the_post_thumbnail(); ?>
            </div>
        </div>

        <div class="info grid">
            <div class="headline">
                <h1><?php $posts_page = get_option('page_for_posts'); echo get_the_title($posts_page); ?></h1>
            </div>
        </div>		
    </section>

    <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

        <article class="post-article grid">
            <section class="article-header">
                <div class="date">
                    <h4 class="h5"><?php the_time('F j, Y'); ?></h4>
                </div>

                <div class="title headline">
                    <h2 class="h2"><?php the_title(); ?></h2>
                </div>

                <div class="meta">
                    <?php if(has_category()): ?>
                        <div class="categories">
                            <div class="headline">
                                <h5 class="h6">Categories</h5>
                            </div>
                            
                            <?php the_category(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </section>

            <section class="article-body copy p2">
                <?php the_content(); ?>
            </section>
            
        </article>
        
    <?php endwhile; endif; ?>

    <?php get_template_part('templates/news/recent-posts'); ?>

<?php get_footer(); ?>